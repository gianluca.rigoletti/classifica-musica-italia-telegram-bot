// const SpotifyWebApi = require("spotify-web-api-node")

// const spotifyApi = new SpotifyWebApi({
//   clientId: "3dce678ff7744957bcd129b544f846c1",
//   clientSecret: "3770fb5dc7384024852b534370da75b6"
// })

// spotifyApi

// spotifyApi.setAccessToken(
//   "BQCUzRwfmyAAHyuh4HYDNw9hG7kb20d6DtW-_g3jfHaLs9-sTZYmhMlaDVGDYQj51RKcv1MfHht3HVqOUoVr8YbYBNig1T6lHOo2EXhqnhxlC1jGoVD0gIkCCI2BtvGPqeyGNTit"
// )

// var authorizeURL = spotifyApi.createAuthorizeURL([], "test-state")
// console.log(authorizeURL)

// spotifyApi.searchTracks("track:HEAVEN UPSIDE DOWN artist:MARILYN MANSON").then(
//   function(data) {
//     console.log(data.body.tracks.items[0].external_urls.spotify)
//   },
//   function(err) {
//     console.log("Something went wrong!", err)
//   }
// )

const axios = require("axios")
const querystring = require("querystring")
require("dotenv").config()
const encodedClientId = new Buffer(process.env.CLIENTID).toString("base64")
const encodedClientSecret = new Buffer(process.env.CLIENTSECRET).toString(
  "base64"
)
const totalEncoded = new Buffer(
  `${process.env.CLIENTID}:${process.env.CLIENTSECRET}`
).toString("base64")
const finalString = `${encodedClientId}:${encodedClientSecret}`
axios({
  method: "post",
  url: "https://accounts.spotify.com/api/token",
  data: querystring.stringify({
    grant_type: "client_credentials"
  }),
  headers: {
    Authorization: `Basic ${totalEncoded}`,
    Accept: "application/json",
    "Content-Type": "application/x-www-form-urlencoded"
  }
})
  .then(response => {
    console.log(response.data)
  })
  .catch(err => {
    console.log(err)
  })
