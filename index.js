const parser = require("rss-parser")
const TeleBot = require("telebot")
const fetch = require("node-fetch")
const outdent = require("outdent")
const table = require("text-table")
const axios = require("axios")
const querystring = require("querystring")

require("dotenv").config()
const bot = new TeleBot(process.env.TOKEN)
const url = "http://api.fimi.it/rss/1"
const limit = 10
const baseUrl = `http://api.fimi.it/chart?section=1&`
const totalEncodedSecret = new Buffer(
  `${process.env.CLIENTID}:${process.env.CLIENTSECRET}`
).toString("base64")
let authToken
const usersMap = new Map()

const intToEmojis = [
  "1️⃣",
  "2️⃣",
  "3️⃣",
  "4️⃣",
  "5️⃣",
  "6️⃣",
  "7️⃣",
  "8️⃣",
  "9️⃣",
  "🔟"
]

const replyMarkup = bot.inlineKeyboard([
  [
    bot.inlineButton("Classifica", {
      callback: "getClassifica"
    })
  ]
])

const getClassifica = async msg => {
  if (!authToken) {
    authToken = await refreshToken()
  }
  const page = usersMap.get(msg.chat.id)
  console.log(page)
  return fetch(`${baseUrl}limit=${limit}&page=${page}`)
    .then(response => response.json())
    .then(async response => {
      const responses = await axios.all(
        response.data.slice(0, 10).map(el => {
          return axios({
            method: "get",
            url: "https://api.spotify.com/v1/search",
            params: {
              q: `${el.author} ${el.title}`,
              type: "track",
              limit: 1
            },
            headers: {
              Authorization: `Bearer ${authToken}`,
              Accept: "application/json"
            }
          })
            .then(response => {
              const link = response.data.tracks.items[0].external_urls.spotify
              return link
            })
            .catch(err => {
              return null
            })
        })
      )
      // console.log(response)
      const elements = response.data
        .slice(0, 10)
        .map((el, index) => {
          const upOrDown = el.prev_pos < el.curr_pos ? "🔻" : "🔺"
          let link

          return outdent`
            *${++index + 10 * page}* ${upOrDown}
            *Titolo* ${el.title}
            *Artista* ${el.author}
            *Etichetta* ${el.label}
            ${responses[index]
              ? `[Ascolta su spotify](${responses[index]})`
              : ""}
          `
        })
        .join("\n\n")
      let reply = `_Settimana dal ${response.chart.date_start} al ${response
        .chart.date_end}_`
      reply += `\n\n${elements}`
      const fullKeyBoard = !page
        ? bot.inlineKeyboard([
            [
              bot.inlineButton(`${page + 1}`, {
                callback: "null"
              }),
              bot.inlineButton(`Successivo`, {
                callback: "increasePage"
              })
            ]
          ])
        : bot.inlineKeyboard([
            [
              bot.inlineButton(`Precedente`, {
                callback: "decreasePage"
              }),
              bot.inlineButton(`${page + 1}`, {
                callback: "null"
              }),
              bot.inlineButton(`Successivo`, {
                callback: "increasePage"
              })
            ]
          ])
      return bot.sendMessage(msg.chat.id, reply, {
        replyMarkup: fullKeyBoard,
        parseMode: "Markdown",
        webPreview: false
      })
    })
    .catch(err => console.log(err))
}

bot.on("/start", msg => {
  usersMap.set(msg.chat.id, 0)
  bot.sendMessage(msg.chat.id, "Scrivi /classifica per iniziare", {
    replyMarkup
  })
})

bot.on("/classifica", msg => {
  usersMap.set(msg.chat.id, 0)
  return getClassifica(msg)
})

// Inline button callback
bot.on("callbackQuery", msg => {
  // User message alert
  let page
  console.log(msg.message.chat.id)
  switch (msg.data) {
    case "increasePage":
      page = usersMap.get(msg.message.chat.id)
      usersMap.set(msg.message.chat.id, page + 1)
      console.log(usersMap.get(msg.message.chat.id))
      return getClassifica(msg.message)
      break
    case "decreasePage":
      page = usersMap.get(msg.message.chat.id)
      usersMap.set(msg.message.chat.id, page - 1)
      return getClassifica(msg.message)
      break
    default:
      return bot.answerCallbackQuery(msg.id)
      console.log("ciao")
      break
  }
})

const refreshToken = () => {
  return axios({
    method: "post",
    url: "https://accounts.spotify.com/api/token",
    data: querystring.stringify({
      grant_type: "client_credentials"
    }),
    headers: {
      Authorization: `Basic ${totalEncodedSecret}`,
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded"
    }
  })
    .then(response => {
      return response.data.access_token
    })
    .catch(err => {
      console.log(err)
      return null
    })
}

const autoRefreshToken = () => {
  setInterval(() => {
    refreshToken()
  }, 3550000)
}

autoRefreshToken()
bot.start()
